/* UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS
 * Facultad de Ingeniería de Sistemas e Informática
 * E.A.P. de Ingeniería de Software
 * Curso: Estructura de Datos
 * Laboratorio: XXX - EjercicioXX
 * Descripción:
 *
 * Autor: Grupo 8
 *
*/

#include <iostream>
#include <string>

struct tNodo;
typedef tNodo* tLista; // tLista es un puntero a un nodo
struct tNodo{
	int dato;
	tLista siguiente;
};

// PROTOTIPOS DE LAS FUNCIONES

int menu();

// Operaciones basicas con listas
tLista crearNodo(int dato);
int obtenerDatoNodo(tLista lista, int pos);
void cambiarDatoNodo(tLista &lista, int pos, int dato);
int sizeLista(tLista lista);

// Algoritmos de Insercion
void insertarInicio(tLista &lista, int dato);
void insertarFinal(tLista &lista, int dato);
void insertarPosicion(tLista &lista,int dato, int pos);

// Algoritmos de Eliminacion
void eliminarPrimero(tLista &lista);
void eliminarUltimo(tLista &lista);
void eliminarPosicion(tLista &lista,int pos);
void eliminarValor(tLista &lista, int dato);
void recorrerLista(tLista lista);
//void EliminarPorValor(tLista & lista,int dato);

// Algoritmos de Ordenamiento
void ordenamientoBurbuja(tLista &lista);
//void ordenamientoSeleccion(tLista &lista);
void quicksort(tLista &lista, int size);
void ordenar(tLista &lista, int primero, int ultimo);
void ordenamientoShell(tLista &lista, int size);

// Algoritmos de Busqueda

using namespace std;

int main(){
	
	int opc,dato,pos;
	
	tLista lista = NULL;
	
	opc = menu();
	
	while(opc!=0){
		switch(opc){
			case 1:
				cout<<"Ingrese valor: "; cin>>dato;
				insertarInicio(lista,dato);
				recorrerLista(lista);
				break;
			case 2:
				cout<<"Ingrese valor: "; cin>>dato;
				insertarFinal(lista,dato);
				recorrerLista(lista);
				break;
			case 3:
				recorrerLista(lista);
				break;
			case 4:
				quicksort(lista,sizeLista(lista));
				recorrerLista(lista);
				break;
			case 5:
				ordenamientoShell(lista,sizeLista(lista));
				recorrerLista(lista);
				break;
			default: 
				cout<<"Ingrese otra opcion"<<endl;
				break;		
		}
		opc = menu();
	}
	
	
	cin.get();
}

int menu() {

    int opc;
    
    cout<<":::::: MENU ::::::"<<endl;
	cout<<"[1] Adicionar al inicio"<<endl;
	cout<<"[2] Adicionar al final"<<endl;
	cout<<"[3] Mostrar"<<endl;
	cout<<"[4] Ordenar por quicksort"<<endl;
	cout<<"[5] Ordenar por Shell"<<endl;
	cout<<"[0] Salir"<<endl;
    
    do{
		cout<<"OPCION: ";
		cin>>opc;	
	}while(opc<0 || 5<opc);
		
    return  opc;
}

tLista crearNodo(int dato){
	tLista nuevo = new tNodo; // new tNodo devuelve una direccion, que se almacena en un puntero
	nuevo->dato = dato;
	nuevo->siguiente = NULL;
	
	return nuevo;	
}

int obtenerDatoNodo(tLista lista, int pos){
	tLista p = lista;
	int contador = 1;
	while(contador < pos && p->siguiente!=NULL){
		p = p->siguiente;
		contador++;
	}
	return p->dato;
	
}

void cambiarDatoNodo(tLista &lista, int pos, int dato){
	tLista p = lista;
	int contador = 1;
	while(contador < pos && p->siguiente!=NULL){
		p = p->siguiente;
		contador++;
	}
	p->dato = dato;
}

void insertarInicio(tLista &lista, int dato){
	tLista nuevo = crearNodo(dato);
	
	if(lista == NULL){		
		lista = nuevo;
	}else{
		nuevo->siguiente = lista;
		lista = nuevo;
	}
}

void insertarFinal(tLista &lista, int dato){
	tLista nuevo = crearNodo(dato);
	
	if(lista == NULL){		
		lista = nuevo;
	}else{
		tLista p = lista;
		while(p->siguiente != NULL){
			p = p->siguiente;
		}
		p->siguiente = nuevo;	
	}
	
}

void insertarPosicion(tLista &lista,int dato,int pos){
			
	if(lista == NULL){
		cout<<"Lista vacia!"<<endl;
	}else{
		if(pos==1){
			insertarInicio(lista,dato);
		}else{
			tLista nuevo = crearNodo(dato);
			tLista p = lista;
			tLista ant = NULL;
			int contador = 1;
			while(p != NULL && contador<pos){
				ant = p;
				p = p->siguiente;
				contador++;
			}
			if(p == NULL){
				cout<<"Posicion invalida!"<<endl;
			}else{
				ant->siguiente = nuevo;
				nuevo->siguiente = p;
			}
		}
	}		
}

void eliminarPrimero(tLista &lista){
	
	if(lista == NULL){
		cout<<"Lista vacia!";
	}else{
		tLista temporal = lista;
        lista = lista->siguiente;
        delete temporal;
	}        	
}

void eliminarUltimo(tLista &lista){
	
	if(lista==NULL){
		cout<<"Lista vacia!";
	}else{
		if(lista->siguiente == NULL){
			delete lista;
			lista = NULL;
		}else{
			tLista p = lista;
			while(p->siguiente->siguiente != NULL){
				p = p->siguiente;
			}
			tLista temporal = p->siguiente;
		    p->siguiente = NULL;
		    delete temporal;
		}
	}
}

void eliminarPosicion(tLista &lista,int pos){
	
	if(lista == NULL){
		cout<<"Lista vacia!"<<endl;
	}else{
		if(pos==1){
			eliminarPrimero(lista);
		}else{
			tLista p = lista;
			tLista ant = NULL;
			int contador = 1;
			while(p!=NULL && contador<pos){
				ant = p;
				p = p->siguiente;
				contador++;
			}
			if(p==NULL){
				cout<<"Posicion invalida!"<<endl;
			}else{
				tLista temporal = p;
				ant->siguiente = p->siguiente;
				delete temporal; 
			}
		}

	}
}

void eliminarValor(tLista &lista, int dato){
	
	tLista p = lista;
	tLista ant = NULL;
	if(p==NULL){
		cout<<"Lista vacia!"<<endl;
	}else{
		bool encontrado = false;
		while(p!=NULL && !encontrado){
			if(p->dato == dato){
				encontrado = true;
			}else{
				ant = p;
				p = p->siguiente;
			}
		}
		if(p==NULL){
			cout<<"Valor no encontrado!"<<endl;
		}else{
			if(p==lista){
				eliminarPrimero(lista);
			}else{
				tLista temporal = p;
				ant->siguiente = p->siguiente;
				delete temporal; 
			}
		}
	}	
}

void recorrerLista(tLista lista){
	tLista p = lista;
	
	while(p != NULL){
		cout<<p->dato<<"->";
		p = p->siguiente;
	}
	cout<<"NULL"<<endl<<endl;
}

// Algoritmos de ordenamiento

void ordenamientoBurbuja(tLista &lista){

	tLista p = lista;
	int pasadas = sizeLista(lista);
	int contador = 1;

	while(contador<pasadas){
		while(p->siguiente != NULL){
			if(p->dato > p->siguiente->dato){
				int aux = p->dato;
				p->dato = p->siguiente->dato;
				p->siguiente->dato = aux;
			}
			p = p->siguiente;		
		}
		p = lista; // vuelve al inicio
		contador++;
	}
	
}

void quicksort(tLista &lista, int size){
	
	ordenar(lista,1,size);
	
}

void ordenar(tLista &lista, int primero, int ultimo){
	
	int i,j,central;
	int pivote;
	central = (primero+ultimo)/2;
	pivote = obtenerDatoNodo(lista,central);
	i = primero;
	j = ultimo;
	
	do{
		while(obtenerDatoNodo(lista,i)<pivote) i++; // Avanza una posicion hacia la derecha mientras el dato sea menor
		while(obtenerDatoNodo(lista,j)>pivote) j--; // Avanza una posicion hacia la izquierda mientras el dato sea mayor
		
		if(i<=j){ // Mientras los indices no se solapen, hay que intercambiar los valores
			int aux;
			aux = obtenerDatoNodo(lista,i);
			cambiarDatoNodo(lista,i,obtenerDatoNodo(lista,j));
			cambiarDatoNodo(lista,j,aux);
			i++; // Avanza una posicion hacia la derecha
			j--; // Avanza una posicion hacia la izquierda
		}
		
	}while(i<=j); // Mientras los indices no se solapen se repite el proceso
	
	// Ahora ya tenemos todos los valores menores al pivote en la parte izquierda y los mayores en la parte derecha
	
	if(primero<j){
		ordenar(lista,primero,j); // Llamada recursiva enviando la sublista izquierda
	}
	if(i<ultimo){
		ordenar(lista,i,ultimo); // Llamada recursiva enviando la sublista derecha
	}
}

void ordenamientoShell(tLista &lista, int size){
	int intervalo,i,j,k;
	intervalo = size/2;
	
	while(intervalo > 0){
		for(i = intervalo+1;i<=size;i++){
			j = i - intervalo;
			while(j>0){
				k = j + intervalo;
				if(obtenerDatoNodo(lista,j)<=obtenerDatoNodo(lista,k)){
					j = -1;
				}else{
					int aux;
					aux = obtenerDatoNodo(lista,j);
					cambiarDatoNodo(lista,j,obtenerDatoNodo(lista,k));
					cambiarDatoNodo(lista,k,aux);
					j = j - intervalo;
				}
			}
		}
		intervalo = intervalo/2;
	}
}


int sizeLista(tLista lista){
	tLista p = lista;
	if(p==NULL){
		return 0;
	}else{
		int contador = 1;
		while(p->siguiente!=NULL){
			p = p->siguiente;
			contador++;
		}
		return contador;
	}	
	
}

